#!/usr/bin/python3
"""
This is a pumper package meta
"""
#******************************************************************************
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
# licensed under the Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#     http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v2 for more details.
# Author: Myeuler
# Create: 2020-06-30
# Description: the meta info of package
# ******************************************************************************/

from pprint import pprint
from os import path
import yaml
import sys
import os
from abc import ABC, abstractmethod

class __Meta(ABC):
    def __init__(self):
        return None
    @abstractmethod
    def GetMetaVersion():
        pass

class Meta_V1(__Meta):
    main_pkg_meta = {
            "MetaVersion" : "V1",
            "Global"        : [],
            "Name"          : "",
            "License"       : "",
            "Version"       : "",
            "Release"       : "",
            "Group"         : "",
            "Summary"       : "",
            "URL"           : "",
            "BuildRoot"     : "",
            "Autoreq"       : "",
            "Autoprov"      : "",
            "Description"   : {
                                "append" : "",
                                "value" : []
                                },
            "Source"        : [],
            "Patch"         : [],
            "Requires"      : [],
            "BuildRequires" : [],
            "BuildArch"     : "",
            "Conflicts"     : [],
            "Obsoletes"     : [],
            "Provides"      : [],
            "Packages"      : {},
            "Prepare"       : [],
            "Build"         : [],
            "Check"         : [],
            "Clean"         : [],
            "Pre"           : {
                                "append" : "",
                                "value" : []
                                },
            "Post"          : {
                                "append" : "",
                                "value" : []
                                },
            "Postun"        : {
                                "append" : "",
                                "value" : []
                                },
            "Install"       : [],
            "FileList"      : [],
            "Changelog"     : [],
            "Files"         : {
                                "append" : "",
                                "value" : []
                                },
            }
    sub_pkg_meta = {
            "Name"          : "",
            "Summary"       : "",
            "Description"   : {
                                "append" : "",
                                "value" : []
                                },
            "Requires"      : [],
            "BuildRequires" : [],
            "BuildArch"     : "",
            "Conflicts"     : [],
            "Obsoletes"     : [],
            "Provides"      : [],
            "Files"         : []
            }

    def __init__(self):
        return None

    def GetMetaVersion(self):
        return self.main_pkg_meta["MetaVersion"]

    def GetPkgMeta(self):
        return self.main_pkg_meta

    def GetSubMeta(self):
        return self.sub_pkg_meta

    def Name(self):
        return ""
    def License(self):
        return ""
    def Global(self):
        return []
    def BuildArch(self):
        return ""
    def Conflicts(self):
        return []
    def Build(self):
        return []
    def ChangeLog(self):
        return []
    def BuildRequires(self):
        return []

