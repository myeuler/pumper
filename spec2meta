#!/usr/bin/python3
"""
This is a tool to turn spec file to be meta yaml file
"""
#******************************************************************************
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
# licensed under the Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#     http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v2 for more details.
# Author: Myeuler
# Create: 2020-06-30
# Description: provide a tool to link upstream community, spec and pkgs
# ******************************************************************************/

import re
import os
import sys
import datetime
import argparse
import subprocess
import platform
import specparser
from pprint import pprint


def do_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("-o", "--output", help="Output to file", type=str, default="")
    parser.add_argument("-m", "--missing", help="List missing lines", action="store_true")
    parser.add_argument("-y", "--yaml", help="output as yaml format", action="store_true")
    parser.add_argument("spec", type=str, help="The spec file")

    return parser


if __name__ == "__main__":

    parser = do_args()

    args = parser.parse_args()

    if (os.path.exists(args.spec) == False):
        print("Spec file %s does not exist\n" % args.spec)
        sys.exit(1)

    parser = specparser.SpecParser()
    if (parser.ParseFile(args.spec) == False):
        sys.exit(1)

    if (args.missing):
        parser.PrintMissing()
    elif (args.yaml):
        parser.Meta2Yaml()
    else:
        parser.PrintMeta()

    sys.exit(0)

