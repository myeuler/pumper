#!/usr/bin/python3
"""
This is a pumper parser engine
"""
#******************************************************************************
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
# licensed under the Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#     http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v2 for more details.
# Author: Myeuler
# Create: 2020-06-30
# Description: the engine of pumper
# ******************************************************************************/

import urllib
import urllib.request
from pprint import pprint
from os import path
import yaml
import sys
import datetime
import argparse
import subprocess
import os
import platform
from pathlib import Path

class Converter:
    __yaml_file = ""
    __spec_file = ""
    __meta_data = None
    __parser = None

    def __init__(self):
        """
        init class
        """
        return None 

    def __meta_valid(self):
        return True;

    def load_meta(self, meta_file):
        try:
            __meta_data = yaml.load(open(meta_file), Loader=yaml.Loader)
        except FileNotFoundError:
            print("File %s does not exist\n" % meta_file)
            return False
        
        if (__meta_data.get("version") == None):
            print("There is no version info in meta data\n")
            return False
        else:
            if (__meta_data["verison"] == "v1"):
               # __parser = MetaParser_v1()
               return None
            else:
                print("Unknown version meta file\n")
                return False

        return self.__meta_valid();
    

    def output_spec(self):
        if (__meta_data == None):
            print("Please load meta file\n")
            return False

    def spec_to_meta(self, spec_file):
        if (os.path.exists(spec_file) == False):
            print("spec file %s does not exist\n" % spec_file)
            return False

        __spec_file = spec_file
        spec = Spec.from_file(spec_file)
        pprint(vars(spec))

        return True
    

