#!/usr/bin/python3
"""
This is turn meta to spec
"""
#******************************************************************************
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
# licensed under the Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#     http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v2 for more details.
# Author: Myeuler
# Create: 2020-06-30
# Description: the engine of pumper
# ******************************************************************************/

import sys
import os
import platform
import yaml
from pprint import pprint
from os import path
from pathlib import Path
from pkgmeta import Meta_V1 as metav1

class SpecBuilder:
    __meta = None
    __yaml_file = ""
    __spec_file = ""
    mdata = None
    __parser = None

    def __init__(self):
        """
        init class
        """
        self.__meta = metav1()
        self.mdata = self.__meta.GetPkgMeta()
        return None 

    def __meta_valid(self):
        return True;

    def LoadMetaFile(self, meta_file):
        try:
            self.mdata = yaml.load(open(meta_file), Loader=yaml.Loader)
        except FileNotFoundError:
            print("File %s does not exist\n" % meta_file)
            return False

        self.__meta_file = meta_file
        
        mv = self.__meta.GetMetaVersion()
        if (mv == None):
            print("There is no version info in meta data\n")
            return False
        elif (mv == "V1"):
            # __parser = MetaParser_v1()
            return True
        else:
            print("Unknown version meta file\n")
            return False

        return self.__meta_valid();

    def __print_list(self, name, lst):
        if len(lst) is not 0:
            for b in lst:
                print(name + ": " + b)

    def __print_changelog(self, name, append, blob):
        prt = "%" + name
        for i in append:
            prt = prt + " " + i
        stx = True
        for i in blob:
            if i[0] == "*" and stx is not True:
                self.__print_sep()
            print(i)
            stx = False

    def __print_blob(self, name, append, blob):
        return self.__print_blob_with_sep(name, append, blob, False)

    def __print_blob_sep(self, name, append, blob):
        return self.__print_blob_with_sep(name, append, blob, True)

    def __print_blob_with_sep(self, name, append, blob, sep):
        prt = "%" + name
        prt = prt + " " + append
        print(prt)
        for i in blob:            
            print(i)
            if sep is True:
                self.__print_sep()


    def __print_sep(self):
        print("")
        return None

    def BuildSpec(self, output):
        if (self.mdata == None):
            print("Please load meta file\n")
            return False

        tmp = sys.stdout 
        if os.path.isdir(output):
            output = os.path.join(output, self.mdata["Name"], ".spec")
        elif output != "":
            sys.stdout = open(output, 'w+')

        for g in self.mdata["Global"]:
            i = 0
            for c in g:
                if i == 0:
                    print("%global " + c)
                else:
                    print(c)
                i = i + 1

        self.__print_sep()
        print("Name: " + self.mdata["Name"])
        print("Version: " + self.mdata["Version"])
        print("Release: " + self.mdata["Release"])
        print("Summary: " + self.mdata["Summary"])
        print("License: " + self.mdata["License"])
        print("URL: " + self.mdata["URL"])
   
        self.__print_sep()

        for r in self.mdata["Source"]:
            for s in r:
                print("Source" + s + ": " + r[s])
        for r in self.mdata["Patch"]:
            for s in r:
                print("Patch" + s + ": " + r[s])

        if self.mdata["BuildArch"] is not "":
            print("BuildArch: " + self.mdata["BuildArch"])

        if self.mdata["BuildRoot"] is not "":
            print("BuildRoot: " + self.mdata["BuildRoot"])

        self.__print_sep()
        self.__print_list("BuildRequires", self.mdata["BuildRequires"])
        self.__print_sep()
        self.__print_list("Requires", self.mdata["Requires"])
        self.__print_sep()
        self.__print_list("Conflicts", self.mdata["Conflicts"])
        self.__print_sep()
        self.__print_list("Obsoletes", self.mdata["Obsoletes"])
        self.__print_sep()
        self.__print_list("Provides", self.mdata["Provides"])

        self.__print_sep()
        print("%description")
        for d in self.mdata["Description"]:
            print(d)

        self.__print_sep()
        for p in self.mdata["Packages"]:
            info = self.mdata["Packages"][p]
            print("%package " + info["append"])
            print("Summary: " + info["Summary"])
            self.__print_list("Provides", info["Provides"])
            self.__print_list("Conflicts", info["Conflicts"])
            self.__print_list("Obsoletes", info["Obsoletes"])
            self.__print_list("Requires", info["Requires"])
            self.__print_list("BuildRequires", info["BuildRequires"])
            desc = info["Description"]
            self.__print_blob("description", desc["append"], desc["value"])
           
        self.__print_sep()
        self.__print_blob("prep", "", self.mdata["Prepare"])
        self.__print_sep()
        self.__print_blob("build", "", self.mdata["Build"])
        self.__print_sep()
        self.__print_blob("install", "", self.mdata["Install"])
        self.__print_sep()
        self.__print_blob("check", "", self.mdata["Check"])
        self.__print_sep()
        self.__print_blob("clean", "", self.mdata["Clean"])
        self.__print_sep()
        self.__print_blob("pre", self.mdata["Pre"]["append"], self.mdata["Pre"]["value"])
        self.__print_sep()
        self.__print_blob("post", self.mdata["Post"]["append"], self.mdata["Post"]["value"])
        self.__print_sep()
        self.__print_blob("postun", self.mdata["Postun"]["append"], self.mdata["Postun"]["value"])

        self.__print_sep()
        self.__print_blob("files", self.mdata["Files"]["append"], self.mdata["Files"]["value"])
        for p in self.mdata["Packages"]:
            self.__print_sep()
            info = self.mdata["Packages"][p]
            self.__print_blob("files", info["Files"]["append"], info["Files"]["value"])
        
        self.__print_sep()
        self.__print_changelog("changelog", "", self.mdata["Changelog"])



