#!/usr/bin/python3
"""
This is a parser to parse rpm spec file
"""
#******************************************************************************
# Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
# licensed under the Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#     http://license.coscl.org.cn/MulanPSL2
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v2 for more details.
# Author: Myeuler
# Create: 2020-06-30
# Description: spec parser tool
# ******************************************************************************/

import re
import os
import yaml
import pkgmeta
from pprint import pprint
from abc import ABC, ABCMeta, abstractmethod

class __KeyWord(ABC):
    def __init__(self,  key, pattern):
        self.__pattern = pattern
        self.key = key
        return None

    def Match(self, line):
        return re.match(self.__pattern, line)

    @abstractmethod
    def DoParse(self, fd, line, meta):
        pass

class Key_1_Value_1(__KeyWord):
    def __init__(self, key, pattern):
        return super().__init__(key, pattern)

    def DoParse(self, fd, line, meta):
        if (self.key not in meta):
            raise Exception("KV-1-1 : Key %s is not supported\n" % self.key)
        obj = super().Match(line)
        meta[self.key] = obj[1]
        return None

class Key_N_Value_1(__KeyWord):
    def __init__(self, key, pattern):
        return super().__init__(key, pattern)

    def DoParse(self, fd, line, meta):
        if (self.key not in meta):
            raise Exception("KV-N-1 : Key %s is not supported\n" % self.key)
        obj = super().Match(line)
        meta[self.key].append({obj[1] : obj[2]})
        return None

class Key_1_Value_N(__KeyWord):
    def __init__(self, key, pattern, spliter):
        self.__spliter = spliter
        return super().__init__(key, pattern)

    def DoParse(self, fd, line, meta):
        obj = super().Match(line)
        if (self.__spliter is not None):
            substrs = self.__spliter(obj[1])
        else:
            substrs = [obj[1]]
        if (self.key not in meta):
            raise Exception("KV-1-N : Key %s is not supported\n" % self.key)
        for substr in substrs:
            meta[self.key].append(substr)
        return None


class SegmentIF(__KeyWord):
    def __init__(self, key, pattern, handler):
        self.__handler = handler
        return super().__init__(key, pattern)

    def DoParse(self, fd, line, meta):
        obj = super().Match(line)
        val = self.__handler(fd, obj[1])
        meta["MacroBlob"].append(val)
        return None

class MultiLines(__KeyWord):
    def __init__(self, key, pattern, handler, boarder):
        self.__handler = handler
        self.__boarder = boarder
        return super().__init__(key, pattern)
    
    def DoParse(self, fd, line, meta):
        if (self.key not in meta):
            raise Exception("ML : Key %s is not supported\n" % self.key)
        obj = super().Match(line)
        val = self.__handler(fd, obj[1], self.__boarder)
        meta[self.key].append(val)
        return None

class SegmentBlob(__KeyWord):
    def __init__(self, key, pattern, handler, boarder):
        self.__handler = handler
        self.__boarder = boarder
        return super().__init__(key, pattern)

    def DoParse(self, fd, line, meta):
        obj = super().Match(line)
        name, val = self.__handler(fd, obj[1], self.__boarder)
        if (name == None):
            # if name is none, that means that may contain macros
            return None
        elif (name == ""):
            # there is no name, that means this is the main pkg blob
            if (self.key not in meta):
                meta[self.key] = {}
            meta[self.key]["append"] = obj[1]
            meta[self.key]["value"] = val
            return None
        else:
            # package has not been created, create it
            pkg = meta["Packages"]
            if (name not in pkg):
                pkg[name] = {}
            pkg[name][self.key] = {}
            pkg[name][self.key]["append"] = obj[1]
            pkg[name][self.key]["value"] = val
        return None


class SegmentPkg(__KeyWord):
    def __init__(self, key, pattern, handler, boarder):
        self.__handler = handler
        self.__boarder = boarder
        return super().__init__(key, pattern)

    def DoParse(self, fd, line, meta):
        obj = super().Match(line)
        name, ret = self.__handler(fd, obj[1], self.__boarder)
        if (name == ""):
            return None
        else:
            pkg = meta["Packages"]
            if (name not in pkg):
                pkg[name] = {}
                pkg[name]["append"] = obj[1]
            for k, v in ret.items():
                pkg[name][k] = v

        return None

class Blob(__KeyWord):
    def __init__(self, key, pattern, handler, boarder):
        self.__handler = handler
        self.__boarder = boarder
        return super().__init__(key, pattern)

    def DoParse(self, fd, line, meta):
        if (self.key not in meta):
            raise Exception("Blob : Key %s is not supported\n" % self.key)
        obj = super().Match(line)
        val = self.__handler(fd, line, self.__boarder)
        meta[self.key] = val
        return None


class SpecParser():
    __missing_lines = []
    keywords = {}
    __meta = pkgmeta.Meta_V1().GetPkgMeta()
    #
    # Do not include %if %endif into boundary. if the %if block is included
    # in those boundary, just include them as part of the blob. 
    # The only need to handled is out of those blobs
    #
    __boarder = [re.compile(r"^%package\s*.*"),
              re.compile(r"^%description\s*.*"),
              re.compile(r"^%prep\s*.*"),
              re.compile(r"^%build\s*.*"),
              re.compile(r"^%check\s*.*"),
              re.compile(r"^%clean\s*.*"),
              re.compile(r"^%install\s*.*"),
              re.compile(r"^%pre\s*.*"),
              re.compile(r"^%post\s*.*"),
              re.compile(r"^%preun\s*.*"),
              re.compile(r"^%postun\s*.*"),
              re.compile(r"^%posttrans\s*.*"),
              re.compile(r"^%files\s*.*"),
              re.compile(r"^%changelog\s*.*")
             ]

    def __init__(self):
        self.__boarder_with_if = self.__boarder[:]
        self.__boarder_with_if.append(re.compile(r"^%if\s*.*"))

        self.keywords['Name'] = Key_1_Value_1("Name", re.compile(r"^Name\s*:\s*(\S*)"))
        self.keywords['Version'] = Key_1_Value_1("Version", re.compile(r"^Version\s*:\s*(\S+)"))
        self.keywords['Release'] = Key_1_Value_1("Release", re.compile(r"^Release\s*:\s*(\S+)"))
        self.keywords['Summary'] = Key_1_Value_1("Summary", re.compile(r"^Summary\s*:\s*(.+)"))
        self.keywords['License'] = Key_1_Value_1("License", re.compile(r"^License\s*:\s*(.+)"))
        self.keywords['URL'] = Key_1_Value_1("URL", re.compile(r"^URL\s*:\s*(\S+)"))
        self.keywords['Group'] = Key_1_Value_1("Group", re.compile(r"^Group\s*:\s*(.+)"))
        self.keywords['BuildRoot'] = Key_1_Value_1("BuildRoot", re.compile(r"^BuildRoot\s*:\s*(.+)"))
        self.keywords['BuildArch'] = Key_1_Value_1("BuildArch", re.compile(r"^BuildArch\s*:\s*(\S+)"))
        self.keywords['Autoreq'] = Key_1_Value_1("Autoreq", re.compile(r"^Autoreq\s*:\s*(.+)"))
        self.keywords['Autoprov'] = Key_1_Value_1("Autoprov", re.compile(r"^Autoprov\s*:\s*(.+)"))

        self.keywords['Source'] = Key_N_Value_1("Source", re.compile(r"^Source(\d*)\s*:\s*(\S+)"))
        self.keywords['Patch'] = Key_N_Value_1("Patch", re.compile(r"^Patch(\d*)\s*:\s*(\S+)"))

        self.keywords['Requires'] = Key_1_Value_N("Requires", re.compile(r"^Requires\s*:\s*(.+)"), self.__brcop_split_value)
        self.keywords['BuildRequires'] = Key_1_Value_N("BuildRequires", re.compile(r"^BuildRequires\s*:\s*(.+)"), self.__brcop_split_value)
        self.keywords['Conflicts'] = Key_1_Value_N("Conflicts", re.compile(r"^Conflicts\s*:\s*(.+)"), self.__brcop_split_value)
        self.keywords['Obsoletes'] = Key_1_Value_N("Obsoletes", re.compile(r"^Obsoletes\s*:\s*(.+)"), self.__brcop_split_value)
        self.keywords['Provides'] = Key_1_Value_N("Provides", re.compile(r"^Provides\s*:\s*(.+)"), self.__brcop_split_value)


        #
        # For prepare, build macro, %prep xxx is valid, the spec consider this format is OK although xxx 
        # will not be parsed, So, to incase some spec has this, add "\s*.+" to match everything after %prep
        # But the Blob does not handle the rest part of the matching
        #
        self.keywords['Prepare'] = Blob("Prepare", re.compile(r"^%prep\s*"), self.__strip_blob_hdr, self.__boarder)
        self.keywords['Build'] = Blob("Build", re.compile(r"^%build\s*"), self.__strip_blob_hdr, self.__boarder)
        self.keywords['Install'] = Blob("Install", re.compile(r"^%install\s*"), self.__strip_blob_hdr, self.__boarder)
        self.keywords['Clean'] = Blob("Clean", re.compile(r"^%clean\s*"), self.__strip_blob_hdr, self.__boarder)
        self.keywords['Pre'] = Blob("Pre", re.compile(r"^%pre$"), self.__strip_blob_hdr, self.__boarder)
        self.keywords['Post'] = Blob("Post", re.compile(r"^%post$"), self.__strip_blob_hdr, self.__boarder)
        self.keywords['Postun'] = Blob("Postun", re.compile(r"^%postun$"), self.__strip_blob_hdr, self.__boarder)
        self.keywords['Check'] = Blob("Check", re.compile(r"^%check\s*"), self.__strip_blob_hdr, self.__boarder)
        self.keywords['Changelog'] = Blob("Changelog", re.compile(r"^%changelog\s*"), self.__strip_blob_hdr, self.__boarder)
        self.keywords['FileList'] = Blob("FileList", re.compile(r"^%files$"), self.__strip_blob_hdr, self.__boarder)

        self.keywords['Define'] = MultiLines("Define", 
                                                re.compile(r"^%define\s*(\S+.*)"), 
                                                self.__multi_lines,
                                                self.__boarder_with_if)
        self.keywords['Global'] = MultiLines("Global", 
                                                re.compile(r"^%global\s*(\S+.*)"), 
                                                self.__multi_lines,
                                                self.__boarder_with_if)

        self.keywords['Package'] = SegmentPkg("Package", 
                                                re.compile(r"^%package\s*(.+)"), 
                                                self.__package_hdr, 
                                                self.__boarder_with_if)

        self.keywords['DescPkg'] = Blob("Description", 
                                        re.compile(r"^%description$"), 
                                        self.__strip_blob_hdr, 
                                        self.__boarder_with_if)

        self.keywords['Description'] = SegmentBlob("Description", 
                                                re.compile(r"^%description\s*(.+)"), 
                                                self.__segment_blob_hdr,
                                                self.__boarder_with_if)

        #
        # %pre -p ldconfig
        # %post -p ldconfig devel
        # %postun devel -p ldconfig
        # All of above are valid :-(
        #
        # r"^%pre \s* need to add a space to avoid match "%prep" string
        self.keywords['Pre_Append'] = SegmentBlob("Pre", 
                                                re.compile(r"^%pre \s*(.+)"), 
                                                self.__segment_blob_hdr, 
                                                self.__boarder)
        # r"^%post \s* need to add a space to avoid match "%postun" string
        self.keywords['Post_Append'] = SegmentBlob("Post", 
                                                re.compile(r"^%post \s*(.+)"), 
                                                self.__segment_blob_hdr, 
                                                self.__boarder)
        self.keywords['Postun_Append'] = SegmentBlob("Postun", 
                                                re.compile(r"^%postun \s*(.+)"), 
                                                self.__segment_blob_hdr, 
                                                self.__boarder)

        self.keywords['Files'] = SegmentBlob("Files", 
                                                re.compile(r"^%files \s*(.+)"), 
                                                self.__segment_blob_hdr, 
                                                self.__boarder)

        self.keywords['IfMacro'] = SegmentIF("IfMacro", re.compile(r"%if\s*(.+)"), self.__segment_if_hdr)

        return None

    def __remove_return(self, line):
        return line.strip('\n')


    #
    # Some Requires may like :
    #   perl(:MODULE_COMPAT(eval "%{}"; echo xxx))
    # To simplify the case, any space in () will be considered
    # as a bundle, do not break them into pieces
    #
    def __brcop_as_bundle(self, value):
        count = 0
        has = 0
        for c in value:
            if c == "(":
                count = count + 1
                has = 1
            elif c == ")":
                count = count - 1
            elif c == " ":
                if count > 0:
                    return True
        return False


    #
    # The split function for :
    # BuildRequires, Requires, Conflict, Obsoletes, Provides
    # 
    def __brcop_split_value(self, value):        
        brcops = []
        stack = []
        need_pop = False
        need_link = False
        
        if (self.__brcop_as_bundle(value) is True):
            brcops.append(value)
            return brcops

        subs = re.split("\s+", value)
        if (len(subs) <= 2):
            for sub in subs:
                brcops.append(sub)
        else:
            for sub in subs:
                if (sub in ["=", "<", ">", ">=", "<=", "!="]):
                    #
                    # need to add space to make A < B as the result than A<B
                    # A<B is not valid for Obsoletes and so on
                    #
                    tmp = stack.pop() + " " + sub
                    stack.append(tmp)
                    need_pop = True
                    need_link = True
                elif (need_pop == False):
                    stack.append(sub)
                    need_pop = True
                else:
                    if (need_link):
                        brcops.append(stack.pop() + " " + sub)
                        need_pop = False
                        need_link = False
                    else:
                        brcops.append(stack.pop())
                        stack.append(sub)
                        need_pop = True
                        need_link = False

        return brcops 

    def __boundary_end(self, line, boarder):
        for bound in boarder:
            if (re.match(bound, line) is not None):
                return True
        return False

    #
    # if the name is %xx, that's not a real name, that's macro
    # %files -f file-list
    # %post -p /sbin/ldconfig
    # For these cases, this is a main package
    #
    def __get_pkg_name(self, substr):
        name = ""
        subs = re.split("\s+", substr.strip())
        ready = True
        i = 0
        while True:
            if (i >= len(subs)):
                break
            if (re.match(r"^%.*", subs[i]) is not None):
                continue
            if (subs[i] == "-f" or subs[i] == "-p"):
                if (i + 1 >= len(subs)):
                    print("spec contain err in : %s" % substr)
                    sys.exit(1)
                else:
                    i = i + 2
                    continue
            if (subs[i] == "-n"):
                i = i + 1
                if (i >= len(subs)):
                    print("spec contain err in : %s" % substr)
                    sys.exit(1)
                else:
                    return subs[i]
            return subs[i]
        return name

    def __segment_if_hdr(self, fd, append):
        val = {}
        val["Append"] = append
        val["Contents"] = []
        while True:
            line = fd.readline()
            if (not line):
                break
            elif (re.match(r"^%endif\s*", line) is not None):
                break
            else:
                line = self.__remove_return(line)
                val["Contents"].append(line)
        return val

    def __segment_blob_hdr(self, fd, append, boarder):
        val = []
        if (append.strip() == ""):
            return None, val

        name = self.__get_pkg_name(append)
        return name, self.__strip_blob_hdr(fd, append, boarder)

    #
    # package handler need to return 2 info:
    # 1. Name of the package
    # 2. The package info
    #
    def __package_hdr(self, fd, append, boarder):
        val = pkgmeta.Meta_V1().GetSubMeta()
        if (append.strip() == ""):
            return "", val
        name = self.__get_pkg_name(append)
        if (name == ""):
            return "", val
        while True:
            pos = fd.tell()
            line = fd.readline()
            if (not line):
                break;
            if (self.__boundary_end(line, boarder)):
                fd.seek(pos)
                break
            else:
                line = self.__remove_return(line)
            self.__parse(fd, line, val)
            
        return name, val

    #
    # __multi_lines handle those macro which may cross serveral lines
    # For example:
    #   %global __my_info\
    #           This is an Info\
    #           For demo
    # So, The global struct is [ ["a1", "a2", "a3"], ["b1", "b2"]]
    #
    def __multi_lines(self, fd, line, boarder):
        val = []
        val.append(line)
        size = len(line)
        if (line[size-1] != "\\"):
            return val
        while True:
            pos = fd.tell()
            line = fd.readline()
            if (not line):
                break
            elif (self.__boundary_end(line, boarder)):
                #
                # In theory, the multi lines should end before encounter
                # any boarders, but just keep the code here for further improvement
                #
                fd.seek(pos)
                break
            else:
                line = self.__remove_return(line)
            # 
            # if the line is not ended with '\', get out of the loop
            #
            val.append(line)
            size = len(line)
            if (line[size - 1] != "\\"):
                break
        return val    

    
    def __blob_hdr(self, fd, fline, strip, boarder):
        val = []
        while True:
            pos = fd.tell()
            line = fd.readline()
            if (not line):
                break;
            elif (self.__boundary_end(line, boarder)):
                # 
                # if the line is the boundary, need to move the file pointer back
                # to make sure it can be parsed in following parsing job
                #
                fd.seek(pos)
                break
            elif (strip):
                # 
                # Ignore all blank lines
                #
                if (len(line.strip()) == 0):
                    continue
            line = self.__remove_return(line)
            val.append(line)
        return val

    def __strip_blob_hdr(self, fd, line, boarder):
        return self.__blob_hdr(fd, line, True, boarder)

    def __saveall_blob_hdr(self, fd, line, boarder):
        return self.__blob_hdr(fd, line, False, boarder)

    def __parse(self, fd, line, meta):
        for kw in self.keywords:
            res = self.keywords[kw].Match(line)
            if (res == None):
                continue
            else:
                return self.keywords[kw].DoParse(fd, line, meta)
        self.__missing_lines.append(line)


    def ParseFile(self, fname):
        if (os.path.exists(fname) == False):
            print("file %s does not exist\n" % fname)
            return False
    
        with open(fname, "r") as fd:
            while True:
                line = fd.readline()
                if (not line):
                    break
                elif (len(line.strip()) == 0):
                    continue
                else:
                    line = line.strip('\n')
                    line = line.strip()
                try:
                    self.__parse(fd, line, self.__meta)
                except Exception as err:
                    print('An exception happened: %s' % str(err))

    def PrintMissing(self):
        for line in self.__missing_lines:
            print(line)

    def PrintMeta(self):
        pprint(self.__meta)

    def Meta2Yaml(self):
        print(yaml.dump(self.__meta))
        return None

